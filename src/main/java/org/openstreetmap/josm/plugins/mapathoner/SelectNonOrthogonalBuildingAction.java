// License: GPL. For details, see LICENSE file.
package org.openstreetmap.josm.plugins.mapathoner;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.Rectangle;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.data.coor.EastNorth;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.data.osm.Node;
import org.openstreetmap.josm.data.osm.Way;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.Notification;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.Geometry;
import org.openstreetmap.josm.tools.ImageProvider;
import org.openstreetmap.josm.tools.Shortcut;

/**
 * Select Non Orthogonal Building
 *
 * <p>
 * Selects building which are not orthogonal, that is buildings where all
 * corners has greater error than +/- 1 degree from 90 degrees. Corners with
 * more than 10 degrees error are ignored.
 * </p>
 * <p>
 * The non orthogonal buildings found are added to the current selection.
 * </p>
 * <p>
 * This script is open source and licensed under GPL.
 * </p>
 *
 * @author MikeTho16
 * @author qeef
 * @since xxx
 */
public final class SelectNonOrthogonalBuildingAction extends JosmAction
{
    /**
     * Constructs a new {@code SelectNonOrthogonalBuildingAction}.
     */
    public SelectNonOrthogonalBuildingAction()
    {
        super(
            tr("Select Non Orthogonal Buildings"),
            (ImageProvider) null,
            tr("Selects buildings which are not orthogonal."),
            Shortcut.registerShortcut(
                "mapathoner:selectnonorthogonalbuilding",
                tr("Mapathoner: {0}", tr("Select Non Orthogonal Buildings")),
                KeyEvent.VK_Z,
                Shortcut.CTRL_SHIFT
            ),
            true,
            "selectnonorthogonalbuilding",
            true
        );
        putValue("help", ht("/Action/SelectNonOrthogonalBuildings"));
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (!isEnabled())
            return;

        DataSet ds = getLayerManager().getEditDataSet();
        if (ds == null)
            return;

        double circle_building_tolerance = Math.abs(Math.toRadians(Config.getPref().getDouble(
            "mapathoner.circle_building_angle_tolerance_degree",
            5.0)));

        ds.clearSelection();
        for (Way w: ds.getWays()) {
            if (
                !w.isDeleted()
                && w.isArea()
                && w.hasTag("building")
            ) {
                if (!orthogonal(w, circle_building_tolerance))
                    ds.toggleSelected(w);
            }
        }
    }

    /**
     * Decide if all the angles in the way are the same.
     *
     * <p>
     * Yes, this function says nothing about the orthogonality. It
     * compares all the angles-between-three-consecutive-nodes of the
     * way to the angle of the first node, returning true if the
     * difference is less than {@code circle_building_tolerance}.
     * </p>
     * <p>
     * Due to the functionality of this function, it returns true for
     * any regular building, e.g. the circle buildings.
     * </p>
     * <p>
     * Nevertheless, this function also returns true if the building
     * (way) is orthogonal. This is because when the building has
     * different wall lengths, the angle is the same only for the
     * orthogonal buildings. Or really strange ones we hope nobody
     * draws. (Imagine hourglass building. Or rather do not.)
     * </p>
     */
    private static boolean orthogonal(Way w, double circle_building_tolerance)
    {
        List<EastNorth> ens = new ArrayList<>(w.getNodesCount());
        for (Node n: w.getNodes()) {
            if (!n.isLatLonKnown()) {
                return false;
            }
            ens.add(n.getEastNorth());
        }
        // Check angle of index 0 node.
        double first_angle = Math.abs(Geometry.getCornerAngle(
                    ens.get(ens.size() - 2),
                    ens.get(0),
                    ens.get(1)));
        double angle;
        // Check angles of all the other nodes.
        for (int i = 1; i < ens.size() - 1; i++) {
            angle = Math.abs(Geometry.getCornerAngle(
                        ens.get(i - 1),
                        ens.get(i),
                        ens.get(i + 1)));
            if (
                Math.abs(angle - Math.PI) > Math.toRadians(1)
                && Math.abs(angle - first_angle) > circle_building_tolerance
            ) {
                return false;
            }
        }
        return true;
    }
}
